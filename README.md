# 3D distances calculator for Nuke

These executable files generate two versions of the same node. Both calculate distances the same way.

In the "READ ONLY" version, distances are displayed as text instead of inside a numerical knob, therefore they cannot be linked to other knobs/nodes.

These scripts have been tested successfully in **Nuke 11.1v1.**
***

For more info: www.jaimervq.com
